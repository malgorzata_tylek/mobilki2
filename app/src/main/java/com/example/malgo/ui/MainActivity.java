package com.example.malgo.ui;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    EditText editText;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = (EditText) findViewById(R.id.text_view_name);
        button =  (Button) findViewById(R.id.button_show_name);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Toast.makeText(view.getContext(), editText.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_ania:
                if (checked)
                    Toast.makeText(this, "Ania", Toast.LENGTH_SHORT).show();
                    break;
            case R.id.radio_basia:
                if (checked)
                    Toast.makeText(this, "Basia", Toast.LENGTH_SHORT).show();
                    break;
            case R.id.radio_zosia:
                if (checked)
                    Toast.makeText(this, "Zosia", Toast.LENGTH_SHORT).show();
                    break;
        }
    }

    public void onCheckboxClicked(View view) {
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();

        // Check which checkbox was clicked
        switch(view.getId()) {
            case R.id.checkbox_mango:
                if (checked)
                    Toast.makeText(this, "Mango", Toast.LENGTH_SHORT).show();
                break;
            case R.id.checkbox_truskawka:
                if (checked)
                    Toast.makeText(this, "Truskawka", Toast.LENGTH_SHORT).show();

                break;
        }
    }


}
